package org.taruts.gitUtils;

import lombok.SneakyThrows;

import java.net.URI;

public class GitRemoteUrlParser {

    @SneakyThrows
    public static String extractHostAndPort(String remoteUrl) {
        URI hostingUri = extractHostingUri(remoteUrl);
        String host = hostingUri.getHost();
        int port = hostingUri.getPort();
        if (port > 0) {
            return host + ":" + port;
        } else {
            return host;
        }
    }

    @SneakyThrows
    public static URI extractHostingUri(String remoteUrl) {
        if (remoteUrl.startsWith("git@")) {
            String host = extractHostForGitUrl(remoteUrl);
            // It's ok we use http here, hosts redirect to https if they support it
            return new URI("http", host, null, null);
        } else {
            URI uri = new URI(remoteUrl);
            return new URI(uri.getScheme(), null, uri.getHost(), uri.getPort(), null, null, null);
        }
    }

    @SneakyThrows
    public static String extractPath(String remoteUrl) {
        if (remoteUrl.startsWith("git@")) {
            return "/" + remoteUrl.split(":")[1];
        } else {
            return new URI(remoteUrl).getPath();
        }
    }

    private static String extractHostForGitUrl(String remoteUrl) {
        remoteUrl = remoteUrl.substring("git@".length());
        return remoteUrl.split(":")[0];
    }
}
