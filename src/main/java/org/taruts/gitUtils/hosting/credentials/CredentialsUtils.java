package org.taruts.gitUtils.hosting.credentials;

import org.apache.commons.lang3.StringUtils;

public class CredentialsUtils {

    /**
     * There is an inconsistency about passing tokens between two APIs that GitLab provides.
     * The two APIs are:
     * GitLab REST API
     * the HTTP based API that the git utility uses to connect to remotes
     * Both are served by a GitLab instance.
     * GitLab REST API wants tokens to be passed over as usernames.
     * The HTTP based git API wants them to be passed over as passwords.
     * Our plugin extension sees tokens as usernames, the GitLab REST API way.
     */
    public static String[] apiCredentialsToGitUtilityCredentials(String apiUsername, String apiPassword) {
        if (StringUtils.isBlank(apiPassword)) {
            // If the password is not specified, then the username contains a personal access token.
            // We can specify any string as the username, but it has to be specified.
            return new String[]{
                    "token",
                    apiUsername
            };
        } else {
            // We are not authenticating with a personal access token.
            // So, no inconsistencies between APIs here.
            // The username is in username and the password is in password.
            return new String[]{
                    apiUsername,
                    apiPassword
            };
        }
    }
}
