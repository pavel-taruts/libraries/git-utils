package org.taruts.gitUtils.hosting;

import org.taruts.gitUtils.GitRemoteUrlParser;

public class HostingNameExtractor {

    public static String extractHostingName(String hostingOrProjectUrl) {
        String hostAndPort = GitRemoteUrlParser.extractHostAndPort(hostingOrProjectUrl);
        return hostAndPortToHostingName(hostAndPort);
    }

    private static String hostAndPortToHostingName(String hostAndPort) {
        if (hostAndPort == null) {
            return null;
        } else {
            return hostAndPort.replaceAll("[.:]", "-");
        }
    }
}
