package org.taruts.gitUtils;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.taruts.processUtils.ProcessRunner;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class GitUtils {

    public static void cloneOrUpdate(String url, String branch, File directory) {
        cloneOrUpdate(url, null, null, branch, directory);
    }

    public static void cloneOrUpdate(String url, String accessToken, String branch, File directory) {
        cloneOrUpdate(url, accessToken, null, branch, directory);
    }

    @SneakyThrows
    public static void cloneOrUpdate(String url, String username, String password, String branch, File directory) {
        directory = new File(directory.getCanonicalPath());

        if (directory.exists()) {
            try {
                updateWorkingTree(directory);
            } catch (Exception e) {
                log.error("Could not update git repository", e);
                FileUtils.deleteDirectory(directory);
                clone(url, username, password, branch, directory);
            }
        } else {
            clone(url, username, password, branch, directory);
        }
    }

    public static void forceClone(String url, String branch, File directory) {
        forceClone(url, null, null, branch, directory);
    }

    public static void forceClone(String url, String accessToken, String branch, File directory) {
        forceClone(url, accessToken, null, branch, directory);
    }

    @SneakyThrows
    public static void forceClone(String url, String username, String password, String branch, File directory) {
        directory = new File(directory.getCanonicalPath());
        deleteFileIfExists(directory);
        clone(url, username, password, branch, directory);
    }

    public static void clone(String url, String branch, File directory) {
        clone(url, null, null, branch, directory);
    }

    public static void clone(String url, String accessToken, String branch, File directory) {
        clone(url, accessToken, null, branch, directory);
    }

    public static void clone(String url, String username, String password, String branch, File directory) {
        boolean credentialsInUrl = url.startsWith("http") && StringUtils.isNotBlank(username);
        if (credentialsInUrl) {
            url = addCredentialsToGitRepositoryUrl(url, username, password);
        }

        File parentDirectory = directory.getParentFile();
        String directoryName = directory.getName();
        //noinspection ResultOfMethodCallIgnored
        parentDirectory.mkdirs();

        List<String> command = new ArrayList<>(List.of(
                "git", "clone"
        ));
        if (StringUtils.isNotBlank(branch)) {
            command.add("--branch");
            command.add(branch);
        }
        command.add(url);
        command.add(directoryName);

        try {
            ProcessRunner.runProcess(parentDirectory, command);
            // Removing credentials from the remote
            if (credentialsInUrl) {
                url = replaceUserInfoInUrl(url, null);
            }
            ProcessRunner.runProcess(directory, "git", "remote", "set-url", "origin", url);
        } catch (RuntimeException e) {
            // Cleaning the project directory
            if (directory.exists()) {
                try {
                    FileUtils.cleanDirectory(directory);
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
    }

    public static String addCredentialsToGitRepositoryUrl(String url, String username, String password) {
        String userInfoString = getUserInfoString(username, password);
        return replaceUserInfoInUrl(url, userInfoString);
    }

    public static String removeCredentialsFromGitRepositoryUrl(String url) {
        return replaceUserInfoInUrl(url, null);
    }

    private static void updateWorkingTree(File directory) {
        ProcessRunner.runProcess(directory, "git", "fetch");
        ProcessRunner.runProcess(directory, "git", "pull");
    }

    private static void deleteFileIfExists(File directory) {
        if (directory.exists()) {
            try {
                FileUtils.forceDelete(directory);
            } catch (IOException e) {
                String message = (
                        "Could not delete %s. It must be blocked by a process. " +
                                "Restarting the IDE may help"
                ).formatted(directory);
                throw new RuntimeException(message, e);
            }
        }
    }

    private static String getUserInfoString(String username, String password) {
        StringBuilder sb = new StringBuilder(username);
        if (StringUtils.isNotBlank(password)) {
            sb.append(":").append(password);
        }
        return sb.toString();
    }

    @SneakyThrows
    private static String replaceUserInfoInUrl(String url, String userInfoString) {
        var uri = new URI(url);
        URI newUri = replaceUserInfoInUri(uri, userInfoString);
        return newUri.toString();
    }

    @SneakyThrows
    private static URI replaceUserInfoInUri(URI uri, String userInfoString) {
        return new URI(
                uri.getScheme(),
                userInfoString,
                uri.getHost(),
                uri.getPort(),
                uri.getPath(),
                uri.getQuery(),
                uri.getFragment()
        );
    }
}
